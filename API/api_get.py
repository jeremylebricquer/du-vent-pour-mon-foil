import API.api_to_db as api


def get_all():
    tables = ['station', 'status', 'location', 'measure']
    for t in tables:
        return api.request_all_station(t)


def get_station():
    return api.request_all_station('station')


def get_status():
    return api.request_all_station('status')


def get_location():
    return api.request_all_station('location')


def get_measure():
    return api.request_all_station('measure')


def get_table_id(table):
    return api.request_id(table)


def get_oldest_measure_id(limit):
    return api.request_last_measure(limit)


def get_date_measure():
    return api.request_date_measure()


def get_all():
    return api.request_all()
