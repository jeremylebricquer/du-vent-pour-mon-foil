import sqlite3
import requests
from sqlite3 import Error


def get_station_byId(url, id):
    '''
        Get attribute by ID
        params : url + id
        out : JSON to dict
    '''
    response = requests.get(url + id)
    # print(f'Status code: {response.status_code} , Data : {response.json()}')
    res = response.json()['data']
    return res


def select_data(url, id):
    '''
        Pass Json to py dict
        params : url + id
        out : py dict
    '''
    res = get_station_byId(url, id)
    data = {
        "id": res['id'],
        "name": res["meta"]["name"],
        "status": {
            "state": res['status']['state'],
            "date": res['status']['date']
        },
        "location": {
            "latitude": res['location']['latitude'],
            "longitude": res['location']['longitude'],
            "date": res['location']['date']
        },
        "measurements": {
            "wind_heading": res['measurements']['wind_heading'],
            "wind_speed_avg": res['measurements']['wind_speed_avg'],
            "wind_speed_max": res['measurements']['wind_speed_max'],
            "wind_speed_min": res['measurements']['wind_speed_min'],
            "date": res['measurements']['date']
        }
    }
    return data


# creation de la db
connection = sqlite3.connect("Database/base.db")
cursor = connection.cursor()


def create_db():
    '''
    Create TABLES Station, Location, Status, Measure
    '''

    # TABLE STATION
    sql_table_station = """ CREATE TABLE station (
                        id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
                        station_id integer,
                        name TEXT
                    )"""
    cursor.execute(sql_table_station)

    # TABLE STATUS
    sql_table_status = """ CREATE TABLE status (
                        id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
                        state REAL,
                        date TEXT,
                        station_id,
                        FOREIGN KEY (station_id) REFERENCES station (station_id) ON UPDATE SET NULL ON DELETE SET NULL
                )"""
    cursor.execute(sql_table_status)

    # TABLE LOCATION
    sql_table_location = """ CREATE TABLE location (
                        id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
                        date TEXT,
                        latitude REAL,
                        longitude REAL,
                        station_id,
                        FOREIGN KEY (station_id) REFERENCES station (station_id) ON UPDATE SET NULL ON DELETE SET NULL
                )"""
    cursor.execute(sql_table_location)

    # TABLE MEASURE
    sql_table_measure = """ CREATE TABLE measure (
                        id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
                        date TEXT,
                        wind_heading REAL,
                        wind_speed_avg REAL,
                        wind_speed_max REAL,
                        wind_speed_min REAL,
                        station_id,
                        FOREIGN KEY (station_id) REFERENCES station (station_id) ON UPDATE SET NULL ON DELETE SET NULL
                )"""
    cursor.execute(sql_table_measure)

# CRUD


def add_data(data):
    '''
        Create first data into db
    '''
    # Préparation requette , avec la variable contenant le result de l'api

    # Station
    isExist = cursor.execute(
        "SELECT COUNT(*) FROM station WHERE station_id = {0}".format(data['id'])).fetchone()[0]
    if isExist == 0:
        cursor.execute("INSERT INTO station (name,station_id) VALUES (?,?)", [
            data['name'], data['id']])
    # Status
    cursor.execute("INSERT INTO status (state,date,station_id) VALUES (?,?,?)", [
        data["status"]['state'], data["status"]["date"], data['id']])
    connection.commit()
    # Location
    cursor.execute("INSERT INTO location (latitude,longitude,date,station_id) VALUES (?,?,?,?)", [
        data["location"]['latitude'], data['location']['longitude'], data['location']['date'], data['id']])
    connection.commit()
    # Measure
    cursor.execute("INSERT INTO measure (wind_heading,wind_speed_avg,wind_speed_max,wind_speed_min,date,station_id) VALUES (?,?,?,?,?,?)", [
        data["measurements"]["wind_heading"], data["measurements"]["wind_speed_avg"], data["measurements"]["wind_speed_max"], data["measurements"]["wind_speed_min"], data["measurements"]["date"], data['id']])
    connection.commit()


def update_measure(data, id):
    '''
    check rows number & limit 10
    params : data , id
    out : insert into measure column
    '''
    # Measure
    count = cursor.execute("SELECT COUNT(*) FROM measure ").fetchone()[0]
    print('count count ', count)
    if count < 10:
        cursor.execute("INSERT INTO measure (wind_heading,wind_speed_avg,wind_speed_max,wind_speed_min,date,station_id) VALUES (?,?,?,?,?,?)", [
            data["wind_heading"], data["wind_speed_avg"], data["wind_speed_max"], data["wind_speed_min"], data["date"], id])
    connection.commit()


def request_date_measure():
    '''
     Request tables : display date from measure col
    '''
    cursor.execute("SELECT date FROM measure order by date asc")
    data = cursor.fetchall()
    #print(f'date measure', data)
    return data


def request_all_station(table):
    '''
     Request tables : display all atribbutes choosing table
     params : table
    '''
    cursor.execute("SELECT * FROM " + str(table))
    data = cursor.fetchall()
    print(f'{table}', data)
    return data


def request_all():
    '''
     Request tables : Join
    '''
    cursor.execute(""" 
    SELECT S.id , S.station_id,S.name,M.wind_heading,M.wind_speed_avg,M.wind_speed_max,M.wind_speed_min,M.date 
    FROM station AS S join measure as M 
    ON s.station_id = m.station_id
    """)
    data = cursor.fetchall()
    print(f'data', data)
    return data


def request_id(table):
    '''
     Request tables : Select id
    '''
    cursor.execute("SELECT ID FROM " + str(table))
    data = cursor.fetchall()
    # print(f'{table}',data )
    return data


def count_items(table):
    '''
     Request tables : count items
    '''
    cursor.execute("SELECT count(id) FROM " + str(table))
    data = cursor.fetchall()
    #print(f'nb items table {table}', data)
    return data


def request_last_measure(limit):
    '''
     Request tables : Select last measure
    '''
    cursor.execute(
        "SELECT id FROM measure  ORDER BY DATE ASC LIMIT " + str(limit))
    data = cursor.fetchall()
    # print('anciennes mesures: ', data)
    return data


def delete_by_id(table, id):
    """
    Delete a measure by measure id
    :param id: id measure
    """
    cursor.execute('DELETE FROM ' + str(table) + ' WHERE id='+str(id))
    connection.commit()


def delete_oldest():
    """
    Delete a measure by measure id
    :param id: id measure
    """
    cursor.execute(
        'DELETE FROM measure WHERE id = (SELECT MIN(id) FROM measure)')
    connection.commit()


## Open & close

def c_open(db):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db)
    except Error as e:
        print(e)

    return conn


def c_close():
    '''
        To close connection
    '''
    connection.close()


def create_backup():
    """
        Create backup 
    """
    back = sqlite3.connect('Database/backup.db')
    connection.backup(back)
