<h1 align="center" style="font-weight: bold">Y'a-t-il du vent pour mon foil</h1>

<span style="font-style: italic">Jérémy Le bricquer<span>

#

# 1. Récupération de données en temps réel

## 1.1. Structure du projet

### API

- `api_get.py` : helper
- `api_to_db.py` : connexion , création à la db et le CRUD

### Database

- `base.db` : Base de donnée sqlite
- `backup.db` : Backup de la base de données

### script python

- `main.py` : script principal contenant l initialisation , la maj de la base , la backup et les contraintes de save

## 1.2. Lancement

Le programme doit être lancé via le script : ` py main.py`

```bash
>>>  py main.py
```

# 2. Contexte du projet

## 2.1. Description

Après avoir vu les images de l’America’s cup dans la baie d’Auckland, Cléante de Nantes a décidé de se mettre au catamaran avec foil.

Avant d’investir dans son engin high tech, elle a décidé de faire une petite application pour sa montre connectée qui donne en temps réel les dernières mesures sur 3 stations locales disposant d’un anémomètre. Elle souhaite savoir où se diriger pour trouver le vent nécessaire à ses futures activités.

Malheureusement sa montre dispose d’un stockage particulièrement réduit, donc la base de données locale devra être mise à jour régulièrement tout en s’assurant de ne pas dépasser un volume de stockage correspondant aux 10 derniers enregistrements. Enfin, un tantinet paranoïaque, elle souhaite absolument avoir la dernière sauvegarde qui devra être mise à jour régulièrement (une fois tous les 5 enregistrements).

Pourrez-vous aider Cléante à réaliser son projet en moins de 48h chrono?

Contrainte: usage de SQLite sur la montre

Les données pourront être récupérées auprès d’anémomètres connectés: les “pioupiou”s https://www.openwindmap.org/PP308

Ceux-ci disposent d’une API qui peut être interrogée: http://developers.pioupiou.fr/api/live/

Modalités pédagogiques : Groupes de 1 ou 2 apprenants

Critères de performance : Efficience du code

Livrables : La base de données, le code d'actualisation de la base en continu et sa méthode de sauvegarde.

## 2.2. Contenu de la réponse de l'API

```json
{
  "doc": "http://developers.pioupiou.fr/api/live/",
  "license": "http://developers.pioupiou.fr/data-licensing",
  "attribution": "(c) contributors of the Pioupiou wind network <http://pioupiou.fr>",
  "data": {
    "id": 110,
    "meta": {
      "name": "Pioupiou 110"
    },
    "location": {
      "latitude": 51.368464,
      "longitude": 3.458852,
      "date": "2015-08-16T14:26:01.000Z",
      "success": true
    },
    "measurements": {
      "date": "2015-08-17T22:07:27.000Z",
      "pressure": null,
      "wind_heading": 292.5,
      "wind_speed_avg": 14,
      "wind_speed_max": 22.5,
      "wind_speed_min": 7
    },
    "status": {
      "date": "2015-08-17T22:07:27.000Z",
      "snr": 23.03,
      "state": "on"
    }
  }
}
```

# 3. Représentation des données

![image info](./MCD.png)
