import API.api_to_db as api
import API.api_get as api_get
import os
import time

url = "http://api.pioupiou.fr/v1/live/"
station_id = "194"

list_stations = ['194', '116', '334']
max_mesure = 10


def init():
    '''
    Init func 
        - create schema / table and create 
        - 1rst data sqlite
    '''
    api.create_db(),
    for ids in list_stations:
        api.get_station_byId(url, ids),
        api.add_data(api.select_data(url, ids)),


def update():
    '''
    Update func
    - insert fresh data 
    - constrain measure's nb 
    '''
    items = api.count_items('measure')
    n_item = items[0][0]
    end = False
    for ids in list_stations:
        api.get_station_byId(url, ids),
        selected_data = api.select_data(url, ids)

        if n_item <= max_mesure:
            api.update_measure(selected_data['measurements'], ids)

        elif n_item > 0 and n_item == max_mesure:
            # del oldest
            api.delete_oldest()
            # insert new
            api.update_measure(selected_data['measurements'], ids)

        # while not end:
        #api.update_measure(selected_data(url, ids))
        # time.sleep(1800)
        #os.system("start python main.py")


def compare():
    '''
    Compare func
    - check if measure exist 
    - return fresh data if new measure 
    '''
    measure = api_get.get_date_measure()
    list_mesure = []
    for ids in list_stations:
        a_date = api.select_data(url, ids)
    for m in measure:
        list_mesure.append(m[0])
    if str(a_date['measurements']['date']) in list_mesure:
        print('mesure déjà existante')
    else:
        print('ajout de la mesure')
        update()


def backup():
    '''
    Backup func
    - create backup sqlite file if 5 > n_item > 10
    '''
    items = api.count_items('measure')
    n_item = items[0][0]
    if 5 > n_item > 10:
        api.create_backup()
        print('backup created')


def main():
    database = "Database/base.db"
    conn = api.c_open(database)
    with conn:
        compare()
        backup()
        api_get.get_all()
    api.c_close()


'''
 Main script 
  - uncomment init to create db first time 
'''
# init()
main()
